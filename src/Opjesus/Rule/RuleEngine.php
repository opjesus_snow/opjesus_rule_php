<?php

namespace Opjesus\Rule;

/**
 * class RuleEngine
 * @package Opjesus\Rule
 *
 *
 * @author 白一泽(也输)
 * @date 2020/7/31 上午9:54
 */
class RuleEngine
{
    public function doFire(RuleContainer $ruleContainer, Fact $fact)
    {
        /**
         * @var  string $ruleCode
         * @var  BaseRule $rule
         */
        foreach ($ruleContainer->rules as $ruleCode => $rule) {
            $rule->triggerBeforeCondition();
            $resCondition = $rule->condition($fact);
            $rule->triggerAfterCondition();
            $rule->triggerBeforeAction();
            if ($resCondition) {
                $rule->action($fact);
            }
            $rule->triggerAfterAction();
        }
    }
}