<?php

namespace Opjesus\Rule;

/**
 * class OldRule
 * @package Opjesus\Rule
 *
 *
 * @author 白一泽(也输)
 * @date 2020/7/31 上午9:54
 */
class OldRule extends BaseRule
{
    public $code = '年龄检查规则';

    public $description = '如果年龄小于30岁,则推荐游戏为王者荣耀';

    public $config = [
        'condition' => [
            'name',
            'old',
        ],
        'action' => [
            'name',
            'old',
        ]
    ];

    public function condition(Fact $fact)
    {
        $old = $fact->get('old');
        if ($old <= 30) {
            return true;
        }

        return false;
    }

    public function action(Fact $fact)
    {
        // todo 执行匹配成功后的策略
        echo sprintf("%s 年龄为%s,推荐游戏王者荣耀\n", $fact->get('name'), $fact->get('old'));
    }

    public function triggerBeforeCondition()
    {
    }

    public function triggerAfterCondition()
    {
    }

    public function triggerBeforeAction()
    {
    }

    public function triggerAfterAction()
    {
    }
}