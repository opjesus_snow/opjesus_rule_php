<?php

namespace Opjesus\Rule;

/**
 * class Fact
 * @package Opjesus\Rule
 *
 *
 * @author 白一泽(也输)
 * @date 2020/7/31 上午9:54
 */
class Fact
{
    public $data;

    public function get($key)
    {
        return $this->data[$key] ?? null;
    }

    public function initData($data)
    {
        $this->data = $data;
    }
}