<?php

namespace Opjesus\Rule;

/**
 * class RuleContainer
 * @package Opjesus\Rule
 *
 *
 * @author 白一泽(也输)
 * @date 2020/7/31 上午9:54
 */
class RuleContainer
{
    public $rules;

    public function register(BaseRule $rule)
    {
        if (isset($this->rules[$rule->code])) {
            return false;
        }
        $this->rules[$rule->code] = $rule;
        return true;
    }

    public function remove($ruleCode)
    {
        unset($this->rules[$ruleCode]);
    }
}