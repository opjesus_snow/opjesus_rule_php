<?php

namespace Opjesus\Rule;

/**
 * class BaseRule
 * @package Opjesus\Rule
 *
 *
 * @author 白一泽(也输)
 * @date 2020/7/31 上午9:54
 */
class BaseRule
{
    public $code;

    public $description;

    public $config;

    public function condition(Fact $fact)
    {
        return false;
    }

    public function action(Fact $fact)
    {
    }

    public function triggerBeforeCondition()
    {
    }

    public function triggerAfterCondition()
    {
    }

    public function triggerBeforeAction()
    {
    }

    public function triggerAfterAction()
    {
    }

    public function validateConfig(Fact $fact)
    {
        $needFields = array_merge($this->config['condition'], $this->config['condition']);
        foreach ($needFields as $field){
            if (!isset($fact->data[$field])) {
                return false;
            }
        }

        return true;
    }
}